﻿using System;
using System.Net;
using System.Web;
using System.Security.Cryptography;
using System.Text.RegularExpressions;
using Microsoft.WindowsAzure.Mobile.Service.Security;
using Newtonsoft.Json.Linq;

using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.IO;
using System.Net.Http;
using System.Web.Http;
using System.Threading.Tasks;
using PhmeAuthService.DataObjects;
#if USE_PSQL
using PhmeAuthService.DataObjects.PSql;
using Microsoft.WindowsAzure.Mobile.Service;
using PhmeAuthService.Controllers;
#else
using PhmeAuthService.DataObjects.MsSql;
#endif

namespace PhmeAuthService.Auth
{
    internal class PicHitMeHelper
    {
        public const string GeneratedPrefix = "GENERATED_nbIU83Jj_";

        public static ApiServices Services { get; set; }

        public static TimeSpan DefaultTokenLifetime
        {
            get
            {
                return new TimeSpan(30, 0, 0, 0);
            }
        }
        /*
        public static auth_user CreateUserEntity(RegistrationRequest registrationRequest)
        {
            Services.Log.Info("Hallooo");
#if USE_TRACE
            using (Trace.CustomTraceWriter.TraceStartFinish(System.Reflection.MethodBase.GetCurrentMethod()))
#endif
            {
                //check if user already exists
                using (var entities = new PhmeDatabaseEntities())
                {
                    Services.Log.Info("CreateUserEntity" + registrationRequest.username);
                    auth_user user = entities.auth_user.SingleOrDefault(a => a.username == registrationRequest.username);
                    Services.Log.Info("CreateUserEntity2" + registrationRequest.username);
                    if (user == null)
                    {
                        string VOID_URL = "https://pichitmedev.com/api/signup/";
#if USE_TRACE
                        using (Trace.CustomTraceWriter.TraceStartFinish(System.Reflection.MethodBase.GetCurrentMethod().Name + " Calling " + VOID_URL))
#endif
                        {
                            Dictionary<string, string> post_values = new Dictionary<string, string>();
                            post_values.Add("username", registrationRequest.username);
                            post_values.Add("email", registrationRequest.email);
                            post_values.Add("password1", registrationRequest.password);
                            post_values.Add("password2", registrationRequest.password);
                            post_values.Add("first_name", registrationRequest.first_name);
                            post_values.Add("last_name", registrationRequest.last_name);

                            String post_string = "";

                            foreach (KeyValuePair<string, string> post_value in post_values)
                            {
                                post_string += post_value.Key + "=" + HttpUtility.UrlEncode(post_value.Value) + "&";
                            }
                            post_string = post_string.TrimEnd('&');
                            HttpWebRequest objRequest = (HttpWebRequest)WebRequest.Create(VOID_URL);
                            objRequest.Method = "POST";
                            objRequest.ContentLength = post_string.Length;
                            objRequest.ContentType = "application/x-www-form-urlencoded";

                            // post data is sent as a stream
                            StreamWriter myWriter = null;
                            myWriter = new StreamWriter(objRequest.GetRequestStream());
                            myWriter.Write(post_string);
                            myWriter.Close();

                            // returned values are returned as a stream, then read into a string
                            HttpWebResponse objResponse = (HttpWebResponse)objRequest.GetResponse();
                            using (StreamReader responseStream = new StreamReader(objResponse.GetResponseStream()))
                            {
                                string post_response = responseStream.ReadToEnd();
                                JObject jObject = JObject.Parse(post_response);
                                string username = jObject.Value<string>("username");
                                //responseStream.Close();
                                //Console.WriteLine("Response" + post_response);
                                auth_user new_user = entities.auth_user.SingleOrDefault(a => a.username == username);
                                return new_user;
                            }
                        }
                    }
                    else
                    {
                        return user;
                    }
                }
            }
        }
*/
        public static string CreateUserEntity(RegistrationRequest registrationRequest)
        {

#if USE_TRACE
            using (Trace.CustomTraceWriter.TraceStartFinish(System.Reflection.MethodBase.GetCurrentMethod()))
#endif
            {

                string VOID_URL = "https://pichitmedev.com/api/identity_provider_signup/";

                Dictionary<string, string> post_values = new Dictionary<string, string>();
                post_values.Add("username", registrationRequest.username);
                post_values.Add("email", registrationRequest.email);
                post_values.Add("password1", registrationRequest.password);
                post_values.Add("password2", registrationRequest.password);
                post_values.Add("first_name", registrationRequest.first_name);
                post_values.Add("last_name", registrationRequest.last_name);
                post_values.Add("token", registrationRequest.token);
                post_values.Add("token_secret", registrationRequest.token_secret);
                post_values.Add("identity_provider", registrationRequest.identity_provider);
                post_values.Add("identity_provider_user_id", registrationRequest.identity_provider_user_id);

                String post_string = "";

                foreach (KeyValuePair<string, string> post_value in post_values)
                {
                    post_string += post_value.Key + "=" + HttpUtility.UrlEncode(post_value.Value) + "&";
                }
                post_string = post_string.TrimEnd('&');
                HttpWebRequest objRequest = (HttpWebRequest)WebRequest.Create(VOID_URL);
                objRequest.Method = "POST";
                objRequest.ContentLength = post_string.Length;
                objRequest.ContentType = "application/x-www-form-urlencoded";

                // post data is sent as a stream
                StreamWriter myWriter = null;
                myWriter = new StreamWriter(objRequest.GetRequestStream());
                myWriter.Write(post_string);
                myWriter.Close();

                // returned values are returned as a stream, then read into a string
                HttpWebResponse objResponse = (HttpWebResponse)objRequest.GetResponse();
                using (StreamReader responseStream = new StreamReader(objResponse.GetResponseStream()))
                {
                    string post_response = responseStream.ReadToEnd();
                    JObject jObject = JObject.Parse(post_response);
                    string username = jObject.Value<string>("username");
                    //System.Web.HttpContext.Current.Session["username"] = username.ToString();
                    //System.Web.HttpContext.Current.Session["userinfo"] = post_response.ToString();
                    //responseStream.Close();

                    return post_response;
                }
            }
        }

        internal static bool MatchPicHitMeUserLogin(auth_user user, LoginRequest loginRequest)
        {
           
            WebRequest request = WebRequest.Create("https://pichitmedev.com/api/token/create/?username=" + loginRequest.username + "&password=" + loginRequest.password);
            request.Method = "GET";
            request.ContentType = "application/json";
            using (WebResponse response = request.GetResponse())
            {
                using (Stream stream = response.GetResponseStream())
                {

                    // Open the stream using a StreamReader for easy access.
                    StreamReader reader = new StreamReader(stream);
                    // Read the content.
                    string responseFromServer = reader.ReadToEnd();
                    // Display the content.
                    Console.WriteLine (responseFromServer);
                    // Clean up the streams.
                    reader.Close ();
                    stream.Close ();
                    response.Close ();
                    //throw new Exception("RESPONSEN: " + responseFromServer);
                    //if (!validated)
                    //{
                    //  return false;
                    //}
                    return true;
                }
            }             
        }

        public static string CreateUserEntityByLoginProviderUser(LoginProvider loginProvider,
                    ProviderCredentials credentials, string accessToken, string accessTokenSecret = "")
        {
            // TODO Replace this code if automatic user creation is required some code based on PicHitMe private rules.
            var userInfoProvider = loginProvider as IUserInfoProvider;
            if (userInfoProvider == null)
            {
                throw new InvalidOperationException("Unknown user info");
            }

            JObject jObject = userInfoProvider.GetUserInfo(credentials);

            RegistrationRequest registrationRequest = userInfoProvider.CreateRegistrationRequestByUserInfo(jObject);
            string username = GeneratedPrefix + Regex.Replace(credentials.UserId, "[^a-zA-Z0-9]", "_");
            if (registrationRequest.email != "")
            {
                username = registrationRequest.email;
            }
            registrationRequest.username = username;
            registrationRequest.password = Guid.NewGuid().ToString("N").Substring(0, 16);
            registrationRequest.identity_provider_user_id = credentials.UserId.ToString();
            registrationRequest.token = accessToken;
            registrationRequest.token_secret = accessTokenSecret;
            registrationRequest.identity_provider = loginProvider.Name;

            return CreateUserEntity(registrationRequest);
        }

        /*
        public static auth_user CreateUserEntityByLoginProviderUser(LoginProvider loginProvider,
    ProviderCredentials credentials)
        {
            // TODO Replace this code if automatic user creation is required some code based on PicHitMe private rules.
            var userInfoProvider = loginProvider as IUserInfoProvider;
            if (userInfoProvider == null)
            {
                throw new InvalidOperationException("Unknown user info");
            }

            JObject jObject = userInfoProvider.GetUserInfo(credentials);

            RegistrationRequest registrationRequest = userInfoProvider.CreateRegistrationRequestByUserInfo(jObject);
            string username = GeneratedPrefix + Regex.Replace(credentials.UserId, "[^a-zA-Z0-9]", "_");
            if (registrationRequest.email != "")
            {
                username = registrationRequest.email;
            }
            registrationRequest.username = username;
            registrationRequest.password = Guid.NewGuid().ToString("N").Substring(0, 16);

            return CreateUserEntity(registrationRequest);
        }
*/
        public static string Encrypt(string token)
        {
            try
            {
                // Create a new instance of the AesCryptoServiceProvider
                // class.  This generates a new key and initialization
                // vector (IV).
                using (AesCryptoServiceProvider myAes = new AesCryptoServiceProvider())
                {
                    // Encrypt the string to an array of bytes.
                    byte[] encrypted = CryptoAes.EncryptStringToBytes_Aes(token, myAes.Key, myAes.IV);

                    // Decrypt the bytes to a string.
                    string roundtrip = CryptoAes.DecryptStringFromBytes_Aes(encrypted, myAes.Key, myAes.IV);

                    //Display the original data and the decrypted data.
                    Console.WriteLine("Original token:   {0}", token);
                    Console.WriteLine("Round Trip: {0}", roundtrip);
                    //return "original token: " + token + ", myAes.Key:" + Convert.ToBase64String(myAes.Key) + ", roundtrip:" + roundtrip + ", encrypted:" + Convert.ToBase64String(encrypted);
                    return Convert.ToBase64String(encrypted);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Error: {0}", e.Message);
                return e.Message;
            }
        }
    }
}