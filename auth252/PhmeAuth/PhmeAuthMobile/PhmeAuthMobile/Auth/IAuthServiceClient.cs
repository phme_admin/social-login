using System.Threading.Tasks;
using Microsoft.WindowsAzure.MobileServices;
using Newtonsoft.Json.Linq;

namespace PhmeAuthMobile.Auth
{
    public interface IAuthServiceClient
    {
        Task<MobileServiceUser> LoginToFacebookAsync();
        Task<MobileServiceUser> LoginToTwitterAsync();
        Task<MobileServiceUser> LoginToGoogleAsync();
        Task<MobileServiceUser> LoginToMicrosoftAccountAsync();
        Task<MobileServiceUser> LoginToWindowsAzureActiveDirectoryAsync();
        Task<MobileServiceUser> LoginToPicHitMeAsync(string username, string password);
        Task<JToken> GetUserInfo();
        Task<string> RegisterOnPicHitMeAsync(string username, string password, string firstName, string lastName, string email);
        Task Logout();
    }
}