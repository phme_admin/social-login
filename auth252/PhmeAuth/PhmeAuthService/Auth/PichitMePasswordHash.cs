using System;
using System.Security.Cryptography;
using Medo.Security.Cryptography;

public class PichitMePasswordHash  
{
    public const int SALT_BYTE_SIZE = 8;
    public const int HASH_BYTE_SIZE = 32;
    public const int PBKDF2_ITERATIONS = 12000;
    public const int TYPE_INDEX = 0;
    public const int ITERATION_INDEX = 1;
    public const int SALT_INDEX = 2;
    public const int PBKDF2_INDEX = 3;

    public static string HashPassword(string password)
    {
        var cryptoProvider = new RNGCryptoServiceProvider();
        byte[] salt = new byte[SALT_BYTE_SIZE];
        cryptoProvider.GetBytes(salt);

        var hash = _PBKDF2(password, salt, PBKDF2_ITERATIONS,  HASH_BYTE_SIZE);
        string salt_str = Convert.ToBase64String(salt);
        return "pbkdf2_sha256$" + PBKDF2_ITERATIONS + "$" +
               salt_str + "$" +
               Convert.ToBase64String(hash);
    }

    public static bool ValidatePassword(string password, string correctHash)
    {
        char[] delimiter = { '$' };
        var split = correctHash.Split(delimiter);
        var iterations = Int32.Parse(split[ITERATION_INDEX]);
        var salt = Convert.FromBase64String(split[SALT_INDEX]);
        var hash = Convert.FromBase64String(split[PBKDF2_INDEX]);

        var testHash = _PBKDF2(password, salt, iterations, hash.Length);
        return SlowEquals(hash, testHash);
    }

    private static bool SlowEquals(byte[] a, byte[] b)
    {
        var diff = (uint)a.Length ^ (uint)b.Length;
        for (int i = 0; i < a.Length && i < b.Length; i++)
        {
            diff |= (uint)(a[i] ^ b[i]);
        }
        return diff == 0;
    }

    private static byte[] _PBKDF2(string password, byte[] salt, int iterations, int outputBytes)
    {
        string salt_str = Convert.ToBase64String(salt);
        using (var hmac = new HMACSHA256())
        {
	        var df = new Pbkdf2(hmac, password, salt_str, iterations);
            return df.GetBytes(outputBytes);
    	}
	    
	    //using (var hmac = new HMACSHA256()) {
    	//var df = new Pbkdf2(hmac, password,  salt);
	    //var bytes = df.GetBytes();

    	//var pbkdf2 = new Rfc2898DeriveBytes(password, salt);
	    //pbkdf2.IterationCount = iterations;
        //return pbkdf2.GetBytes(outputBytes);
    }

    static byte[] GetBytes(string str)
    {
        byte[] bytes = new byte[str.Length * sizeof(char)];
        System.Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);
        return bytes;
    }

    static string GetString(byte[] bytes)
    {
        char[] chars = new char[bytes.Length / sizeof(char)];
        System.Buffer.BlockCopy(bytes, 0, chars, 0, bytes.Length);
        return new string(chars);
    }
}