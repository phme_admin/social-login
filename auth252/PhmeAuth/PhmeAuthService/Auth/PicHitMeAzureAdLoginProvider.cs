﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Microsoft.IdentityModel.Clients.ActiveDirectory;
using Microsoft.Owin.Security.Twitter.Messages;
using Microsoft.WindowsAzure.Mobile.Service;
using Microsoft.WindowsAzure.Mobile.Service.Security;
using Microsoft.WindowsAzure.Mobile.Service.Security.Providers;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;


namespace PhmeAuthService.Auth
{
    public class PicHitMeAzureAdLoginProvider
        : AzureActiveDirectoryExtendedLoginProvider
        , IUserInfoProvider
        , IClaimsChecker
    {
        public PicHitMeAzureAdLoginProvider(HttpConfiguration config, IServiceTokenHandler tokenHandler) 
            : base(config, tokenHandler)
        {
        }

        public JObject GetUserInfo(ProviderCredentials credentials)
        {
            var result = new JObject();
            var aadCredentials = credentials as AzureActiveDirectoryCredentials;
            if (aadCredentials != null)
            {
                var serviceSettings = Config.DependencyResolver.GetServiceSettingsProvider().GetServiceSettings();
                string clientId = serviceSettings.AzureActiveDirectoryClientId;
                string clientKey = serviceSettings["MS_AadClientKey"];
                string graphResourceId = "https://graph.windows.net";

                var authContext =
                    new AuthenticationContext("https://login.windows.net/" +
                                              HttpUtility.UrlEncode(aadCredentials.TenantId));
                var credential = new ClientCredential(clientId, clientKey);
                var authenticationResult = authContext.AcquireToken(graphResourceId, credential);

                string url = string.Format("{0}/{1}/users/{2}?api-version=1.5"
                    , graphResourceId
                    , HttpUtility.UrlEncode(aadCredentials.TenantId)
                    , HttpUtility.UrlEncode(aadCredentials.ObjectId));
                using (var client = new HttpClient())
                {
                    var request = new HttpRequestMessage(HttpMethod.Get, url);
                    request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", authenticationResult.AccessToken);
                    var response = client.SendAsync(request).Result;
                    response.EnsureSuccessStatusCode();
                    result.Add(Name, JToken.Parse(response.Content.ReadAsStringAsync().Result));
                }
            }

            return result;
        }

        public RegistrationRequest CreateRegistrationRequestByUserInfo(JObject userInfo)
        {
            JToken info = userInfo.GetValue(Name);
            string email = info.Value<string>("mail");
            JToken email2 = info.SelectToken("otherMails[0]");

            var registrationRequest = new RegistrationRequest
            {
                email = email ?? (email2 == null ? "" : email2.ToString()),
                first_name = info.Value<string>("givenName") ?? "",
                last_name = info.Value<string>("surname") ?? ""
            };

            return registrationRequest;
        }

        public override ProviderCredentials CreateCredentials(ClaimsIdentity claimsIdentity)
        {
#if USE_TRACE
            using (Trace.CustomTraceWriter.TraceStartFinish(System.Reflection.MethodBase.GetCurrentMethod()))
#endif
            {
                PicHitMeLoginProvider.LogoutAndRetryIfOtherClaimsIssuer(this, claimsIdentity);

                var aadCredentials = base.CreateCredentials(claimsIdentity) as AzureActiveDirectoryCredentials;
                if (aadCredentials != null)
                {
                    PicHitMeLoginProvider.HandleIdentityProviderUser(this, aadCredentials,
                        aadCredentials.AccessToken, aadCredentials.TenantId);
                }

                return aadCredentials;
            }
        }

        public void ThrowIfOtherClaimsIssuer(ClaimsIdentity claimsIdentity)
        {
            Claim claim = claimsIdentity.FindFirst("aud");
            if (claimsIdentity.Claims.Any() 
                && (claim == null || claim.Value != Config.DependencyResolver.GetServiceSettingsProvider().GetServiceSettings().AzureActiveDirectoryClientId))
            {
                throw new InvalidOperationException("Other Claims Issuer");
            }
        }
    }
}
