﻿using System;
using System.Net;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web.Http;
using Microsoft.WindowsAzure.Mobile.Service.Security;
using Microsoft.WindowsAzure.Mobile.Service.Security.Providers;
using Newtonsoft.Json.Linq;

namespace PhmeAuthService.Auth
{
    public class PicHitMeGoogleLoginProvider
        : GoogleLoginProvider, IUserInfoProvider
    {
        public PicHitMeGoogleLoginProvider(HttpConfiguration config, IServiceTokenHandler tokenHandler)
            : base(config, tokenHandler)
        {
        }

        public JObject GetUserInfo(ProviderCredentials credentials)
        {
            var result = new JObject();
            var googleCredentials = credentials as GoogleCredentials;
            if (googleCredentials != null)
            {
                result.Add(Name,
                    PicHitMeLoginProvider.HttpGet(
                        "https://www.googleapis.com/plus/v1/people/me?alt=json&access_token=" +
                        googleCredentials.AccessToken));
            }

            return result;
        }

        public RegistrationRequest CreateRegistrationRequestByUserInfo(JObject userInfo)
        {
            JToken info = userInfo.GetValue(Name);
            JToken email = info.SelectToken("emails[0].value");

            var registrationRequest = new RegistrationRequest
            {
                email = email == null ? "" : email.ToString(),
                first_name = info.SelectToken("name.givenName").ToString(),
                last_name = info.SelectToken("name.familyName").ToString()
            };

            return registrationRequest;
        }

        public override ProviderCredentials CreateCredentials(ClaimsIdentity claimsIdentity)
        {
#if USE_TRACE
            using (Trace.CustomTraceWriter.TraceStartFinish(System.Reflection.MethodBase.GetCurrentMethod()))
#endif
            {
                PicHitMeLoginProvider.LogoutAndRetryIfOtherClaimsIssuer(this, claimsIdentity);

                var googleCredentials = base.CreateCredentials(claimsIdentity) as GoogleCredentials;
                if (googleCredentials != null)
                {
                    PicHitMeLoginProvider.HandleIdentityProviderUser(this, googleCredentials,
                        googleCredentials.AccessToken);
                }

                return googleCredentials;
            }
        }
    }
}