﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace PhmeAuthMobile
{
    public partial class PicHitMeLoginPage
    {
        public string Username {get; set;}
        public string Password {get; set;}

        public PicHitMeLoginPage()
        {
            InitializeComponent();

            Username = "user1";
            Password = "password1";

            Title = "Login using PicHitMe account";
            LoginButton.Clicked += LoginButton_Clicked;
            BindingContext = this;
        }

        async void LoginButton_Clicked(object sender, EventArgs e)
        {
            Func<Task> action = async () => await App.AuthServiceClient.LoginToPicHitMeAsync(Username, Password);
            await MainPage.ExecuteActionAsync("Logging in via PicHitMe Account",
                action, 
                this, 
                Status);

            await App.Navigation.PopAsync();
        }
    }
}
