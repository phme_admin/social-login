using System;
using System.Threading.Tasks;
using Microsoft.WindowsAzure.MobileServices;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using PhmeAuthMobile.Auth;

namespace PhmeAuthMobile.iOS.Auth
{
    internal class AuthServiceClientIos : AuthServiceClient
    {
        private UIViewController _context;

        public UIViewController Context
        {
            get
            {
#if DEBUG
                if (_context == null)
                {
                    throw new InvalidOperationException("Please call Initialize first");
                }
#endif
                return _context;
            }
        }

        public void Initialize(UIViewController context)
        {
            if (context == null) throw new ArgumentNullException("context");
            _context = context;
        }

        protected override async Task<MobileServiceUser> LoginAsync(MobileServiceAuthenticationProvider provider)
        {
            return await Client.LoginAsync(Context, provider);
        }
    }
}