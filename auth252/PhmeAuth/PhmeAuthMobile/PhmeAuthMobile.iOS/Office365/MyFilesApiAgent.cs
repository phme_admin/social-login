﻿
using MonoTouch.UIKit;
using Microsoft.Office365.OAuth;
using Microsoft.Office365.SharePoint;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using PicHitMe.iOS.Office365;
using System.IO;

namespace PicHitMe.iOS
{
    static class MyFilesApiAgent
    {
        const string MyFilesCapability = "MyFiles";

        public static async Task<IEnumerable<IFileSystemItem>> GetMyFiles(UIViewController context)
        {
            var client = await EnsureClientCreated(context);

            // Obtain files in folder "Shared with Everyone"
            var filesResults = await client.Files["Shared with Everyone"].ToFolder().Children.ExecuteAsync();
            var files = filesResults.CurrentPage.OrderBy(e => e.Name);

            return files;
        }

        public static async Task<bool> FolderExists(string folder, UIViewController context)
        {
            var client = await EnsureClientCreated(context);

            var filesResults = await client.Files.ExecuteAsync();
            List<IFileSystemItem> children = filesResults.CurrentPage.ToList();

            foreach(var f in SplitFolderIntoArray(folder))
            {
                var t = children.OfType<Folder>().FirstOrDefault(e => e.Name.ToLower() == f.ToLower());
                if (t == null)
                    return false;

                children = t.Children.Select(e=>e as IFileSystemItem).ToList();
            }

            return true;
        }

        private static List<PhmeAuthMobile.Folder> _rootFolders = new List<PhmeAuthMobile.Folder>();

        /// <summary>
        /// Gets the folders in the root
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public static async Task<List<PhmeAuthMobile.Folder>> GetRootFoldersAsync(UIViewController context)
        {
            // TODO IMPORTANT Does this get the entire folder structure when we only want the first level?
            var result = new List<PhmeAuthMobile.Folder>();

            try
            {
                var client = await EnsureClientCreated(context);
                var fileResult = await client.Files.ExecuteAsync();
                var folders = fileResult.CurrentPage.OfType<Microsoft.Office365.SharePoint.Folder>();

                foreach (var item in folders)
                {
                    if(!item.Id.Contains("/"))
                    {
                        var folder = new PhmeAuthMobile.Folder()
                            {
                                Name = item.Name,
                                Id = item.Id
                            };

                        Traverse(folder, folders);

                        result.Add(folder);
                        _rootFolders.Add(folder);

                    }
                }
            }
            catch(Exception ex)
            {
                int i = 42;
                throw;
            }

            return result;
        }

        /// <summary>
        /// Finds all descendants of a given folder
        /// </summary>
        /// <param name="folder"></param>
        /// <param name="folders"></param>
        private static void Traverse(PhmeAuthMobile.Folder folder, IEnumerable<Microsoft.Office365.SharePoint.Folder> folders)
        {
            var childProspects = folders.Where(e => e.Id.StartsWith(folder.Id));

            foreach(var prospect in childProspects)
            {
                if (prospect.Id == folder.Id)
                    continue;

                var strippedId = prospect.Id.Substring(folder.Id.Length+1); //, prospect.Id.Length - folder.Id.Length);
                if(!strippedId.Contains("/"))
                {
                    var newFolder = new PhmeAuthMobile.Folder()
                        {
                            Id = prospect.Id,
                            Name = prospect.Name
                        };

                    folder.Children.Add(newFolder);
                    newFolder.Parent = folder;

                    Traverse(newFolder, childProspects);
                }
            }
        }

        public static async Task<List<PhmeAuthMobile.Folder>> GetChildrenOfAsync(PhmeAuthMobile.Folder folder, UIViewController context)
        {

            return folder.Children;

            //var client = await EnsureClientCreated(context);
            //var filesResults = await client.Files.Where(e => e.Id == folder.Id).ExecuteAsync();


            //var query = from f in filesResults.CurrentPage.OfType<Folder>().First().Children.OfType<Folder>()
            //            select new Core.Storage.Folder() { Name = f.Name, Id = f.Id };

            //return query.ToList();
        }

        public static async Task UploadFileAsync(string path, byte[] bytes, UIViewController context)
        {
            var client = await EnsureClientCreated(context);

            var stream = new MemoryStream(bytes);
            var r = await client.Files.AddAsync(path, true, stream);
            
        }

        private static List<string> SplitFolderIntoArray(string path)
        {
            path = path.Replace("/", "\\");
            return path.Split("\\".ToCharArray()).ToList();
        }
    
        public static async Task<SharePointClient> EnsureClientCreated(UIViewController context)
        {
            try
            {
                AuthenticationInfo authInfo = await AuthenticatorHelper.GetAuthenticationInfo(context, false, MyFilesCapability, ServiceIdentifierKind.Capability);
                // TODO Handle the null return of authInfo here
                return new SharePointClient(authInfo.ServiceUri, authInfo.GetAccessToken);
            }
            catch(Exception ex)
            {
                throw;
            }
        }
    }
}
