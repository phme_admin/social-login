﻿using System;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web.Http;
using Microsoft.WindowsAzure.Mobile.Service;
using Microsoft.WindowsAzure.Mobile.Service.Security;
using Microsoft.WindowsAzure.Mobile.Service.Security.Providers;
using Newtonsoft.Json.Linq;

namespace PhmeAuthService.Auth
{
    public class PicHitMeTwitterLoginProvider
        : TwitterLoginProvider, IUserInfoProvider
    {
        public PicHitMeTwitterLoginProvider(HttpConfiguration config, IServiceTokenHandler tokenHandler)
            : base(config, tokenHandler)
        {
        }

        public JObject GetUserInfo(ProviderCredentials credentials)
        {
            var result = new JObject();
            var twitterCredentials = credentials as TwitterCredentials;
            if (twitterCredentials != null)
            {
                ServiceSettingsDictionary serviceSettings =
                    Config.DependencyResolver.GetServiceSettingsProvider().GetServiceSettings();

                var handler = new OAuthMessageHandler.Core.OAuthMessageHandler(
                    serviceSettings.TwitterConsumerKey,
                    serviceSettings.TwitterConsumerSecret,
                    twitterCredentials.AccessToken,
                    twitterCredentials.AccessTokenSecret,
                    new HttpClientHandler()
                    );

                result.Add(Name,
                    PicHitMeLoginProvider.HttpGet("https://api.twitter.com/1.1/account/verify_credentials.json", handler));
            }

            return result;
        }

        public RegistrationRequest CreateRegistrationRequestByUserInfo(JObject userInfo)
        {
            JToken info = userInfo.GetValue(Name);
            var registrationRequest = new RegistrationRequest
            {
                email = "@" + info.Value<string>("screen_name"),
                first_name = info.Value<string>("name"),
                last_name = ""
            };

            return registrationRequest;
        }

        public override ProviderCredentials CreateCredentials(ClaimsIdentity claimsIdentity)
        {
#if USE_TRACE
            using (Trace.CustomTraceWriter.TraceStartFinish(System.Reflection.MethodBase.GetCurrentMethod()))
#endif
            {
                PicHitMeLoginProvider.LogoutAndRetryIfOtherClaimsIssuer(this, claimsIdentity);

                var twitterCredentials = base.CreateCredentials(claimsIdentity) as TwitterCredentials;
                if (twitterCredentials != null)
                {
                    PicHitMeLoginProvider.HandleIdentityProviderUser(this, twitterCredentials,
                        twitterCredentials.AccessToken, twitterCredentials.AccessTokenSecret);
                }

                return twitterCredentials;
            }
        }
    }
}