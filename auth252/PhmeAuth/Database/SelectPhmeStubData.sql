
SELECT        auth_user.id, auth_user.username, auth_user.first_name, auth_user.last_name, auth_user.email, auth_user.password, auth_user.is_active, 
                         main_identity_provider.name Provider, main_user_identity_link.user_id, main_user_identity_link.token, main_user_identity_link.allow_auth, main_user_identity_link.token_secret
FROM            auth_user LEFT JOIN
                         main_user_identity_link ON auth_user.id = main_user_identity_link.auth_user_id LEFT JOIN
                         main_identity_provider ON main_user_identity_link.main_identity_provider_id = main_identity_provider.id
