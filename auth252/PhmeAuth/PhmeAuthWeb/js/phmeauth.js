var PhMeAuth = function() {
    
    var client = new WindowsAzure.MobileServiceClient(
        "https://pichitmeauth.azure-mobile.net/", 
        "MnmCVQjRKXhyzmUUXmqISpsutqSKPH89"
    );
    
    var _loginWithSocial = function(name, success, failed) {
        //PhMeAuth.prototype.logout(function() {
            client.login(name).then(function() {
                if (success !== undefined) {
                    success();
                }
            }, function(error) {
                if (failed !== undefined) {
                    failed(error);
                }
            });
        //});
    };
    
    var _loginAndGetInfo = function(name, success, failed) {
        _loginWithSocial(name, function() {
            _getLoginInfo(function(result) {
                success(result);
            }, function(errorLoginInfo) {
                failed(errorLoginInfo);
            });
        }, function(errorLogin) {
            if (failed !== undefined) {
                failed(errorLogin);
            }
        });
    };
    
    var _getLoginInfo = function(success, failed) {
        var reqBody = {
            body: "",
            request_method: "post"
        };

        client.invokeApi("UserInfo", reqBody).then(function(results) {
            success(results.result);
        }, function(error) {
            failed(error);
        });
    };
    
    PhMeAuth.prototype.loginToFacebook = function(success, failed) {
        _loginAndGetInfo("facebook", success, failed);
    };
    
    PhMeAuth.prototype.loginToTwitter = function(success, failed) {
        _loginAndGetInfo("twitter", success, failed);
    };
    
    PhMeAuth.prototype.loginToGoogle = function(success, failed) {
        _loginAndGetInfo("google", success, failed);
    };
    
    PhMeAuth.prototype.loginToMicrosoftAccount = function(success, failed) {
        _loginAndGetInfo("microsoftaccount", success, failed);
    };
    
    PhMeAuth.prototype.loginToAzureAD = function(success, failed) {
        _loginAndGetInfo("aad", success, failed);
    };
    
    PhMeAuth.prototype.loginToPicHitMe = function(username, password, success, failed) {
        PhMeAuth.prototype.logout(function() {
            var reqBody = {
                body: {"username": username, "password": password},
                request_method: "post"
                
            };
            client.invokeApi("PicHitMeLogin", reqBody).then(function(results) {
                if (results.result) {
                    var token = results.result;
                    var canContinue = false;
                    if (token.user) {
                        client.currentUser = token.user;
                        client.currentUser.mobileServiceAuthenticationToken = token.authenticationToken;
                        canContinue = true;
                    } else if (token.User) {
                        client.currentUser = token.User;
                        client.currentUser.mobileServiceAuthenticationToken = token.AuthenticationToken;
                        canContinue = true;
                    }
                    if (canContinue) {
                        _getLoginInfo(function(result) {
                            success(result);
                        }, function(errorLoginInfo) {
                            failed(errorLoginInfo);
                        });
                    } else {
                        failed("Response has wrong parameters.");
                    }
                } else {
                    failed("Response has wrong parameters.");
                }
                
            }, function(errorLogin) {
                failed(errorLogin);
            });
        });
    };
    
    PhMeAuth.prototype.registerOnPicHitMe = function(username, password, first_name, last_name, email, success, failed) {
        var reqBody = {
            body: {
                "username": username, 
                "password": password,
                "first_name": first_name,
                "last_name": last_name,
                "email": email
            },
            request_method: "post"
        };
        client.invokeApi("PicHitMeRegistration", reqBody).then(function(results) {
            success(results.result);
        }, function(error) {
            failed(error);
        });
    };
    
    PhMeAuth.prototype.logout = function(callback) {
        var reqBody = {
            body: "",
            request_method: "get"
        };
        client.invokeApi("Logout", reqBody).then(function(data) {
            client.logout();
            if (callback !== undefined) {
                callback(data.result);
            }
        });
    };
    
};