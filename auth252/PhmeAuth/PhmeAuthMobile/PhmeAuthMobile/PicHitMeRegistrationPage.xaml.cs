﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhmeAuthMobile
{
    public partial class PicHitMeRegistrationPage
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public PicHitMeRegistrationPage()
        {
            InitializeComponent();

            Username = "user1";
            Password = "password1";
            Email = "user1@one.one";
            FirstName = "User";
            LastName = "One";

            Title = "New PicHitMe account";
            RegistrationButton.Clicked += RegistrationButton_Clicked;
            BindingContext = this;
        }

        async void RegistrationButton_Clicked(object sender, EventArgs e)
        {
            Func<Task> action =
                async () => await App.AuthServiceClient.RegisterOnPicHitMeAsync(Username, Password, FirstName, LastName, Email);
            await MainPage.ExecuteActionAsync("Registering new PicHitMe Account", 
                action, 
                this, 
                Status,
                false);

            await App.Navigation.PopAsync();
        }
    }
}
