﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Microsoft.AspNet.Identity;
using Microsoft.Owin;
using Microsoft.Owin.Security;
using Microsoft.WindowsAzure.Mobile.Service;
using Microsoft.WindowsAzure.Mobile.Service.Security;
using Newtonsoft.Json.Linq;
using Owin;
using PhmeAuthService.DataObjects;
#if USE_PSQL
using PhmeAuthService.DataObjects.PSql;
#else
using PhmeAuthService.DataObjects.MsSql;
#endif

namespace PhmeAuthService.Auth
{
    public class PicHitMeLoginProvider : LoginProvider
    {
        public const string ProviderName = "PicHitMe";

        public static ApiServices Services { get; set; }

        public PicHitMeLoginProvider(IServiceTokenHandler tokenHandler)
            : base(tokenHandler)
        {
            this.TokenLifetime = PicHitMeHelper.DefaultTokenLifetime;
        }

        public override void ConfigureMiddleware(IAppBuilder appBuilder, ServiceSettingsDictionary settings)
        {
            // Not Applicable - used for federated identity flows
        }

        public override ProviderCredentials CreateCredentials(ClaimsIdentity claimsIdentity)
        {
#if USE_TRACE
            using (Trace.CustomTraceWriter.TraceStartFinish(System.Reflection.MethodBase.GetCurrentMethod()))
#endif
            {
                if (claimsIdentity == null)
                {
                    throw new ArgumentNullException("claimsIdentity");
                }

                string username = claimsIdentity.FindFirst(ClaimTypes.NameIdentifier).Value;
                var credentials = new PicHitMeProviderCredentials
                {
                    UserId = this.TokenHandler.CreateUserId(this.Name, username),
                };

                return credentials;
            }
        }

        public override ProviderCredentials ParseCredentials(JObject serialized)
        {
            if (serialized == null) throw new ArgumentNullException("serialized");
            return serialized.ToObject<PicHitMeProviderCredentials>();
        }

        public override string Name
        {
            get { return ProviderName; }
        }

        public static void HandleIdentityProviderUser(LoginProvider loginProvider,
            ProviderCredentials credentials, string accessToken, string accessTokenSecret = "")
        {
            //HandleIdentityProviderUserPrivate(loginProvider, credentials, accessToken, accessTokenSecret);
#if USE_TRACE
            using (Trace.CustomTraceWriter.TraceStartFinish(System.Reflection.MethodBase.GetCurrentMethod()))
#endif
            {
                PicHitMeHelper.CreateUserEntityByLoginProviderUser(loginProvider, credentials, accessToken, accessTokenSecret);
            }
        }

/*        private static void HandleIdentityProviderUserPrivate(LoginProvider loginProvider,
            ProviderCredentials credentials, string accessToken, string accessTokenSecret = "")
        {
#if USE_TRACE
            using (Trace.CustomTraceWriter.TraceStartFinish(System.Reflection.MethodBase.GetCurrentMethod()))
#endif
            {
                using (var entities = new PhmeDatabaseEntities())
                {
#if USE_TRACE
                    using (Trace.CustomTraceWriter.TraceStartFinish(System.Reflection.MethodBase.GetCurrentMethod().Name + " ::2:: " + loginProvider.Name))
#endif
                    {
                        main_identity_provider identityProvider = entities.main_identity_provider.SingleOrDefault(ip => ip.name == loginProvider.Name);
                        if (identityProvider == null)
                        {
                            identityProvider = new main_identity_provider()
                            {
                                name = loginProvider.Name
                            };

                            entities.main_identity_provider.Add(identityProvider);
                        }

#if USE_TRACE
                        using (Trace.CustomTraceWriter.TraceStartFinish(System.Reflection.MethodBase.GetCurrentMethod().Name + " ::3:: "))
#endif
                        {
                            main_user_identity_link userIdentityLink = entities.main_user_identity_link.Where(
                                uil => uil.identity_provider_id == identityProvider.id && uil.user_id == credentials.UserId)
                                .Include(uil => uil.auth_user).SingleOrDefault();

                            if (userIdentityLink == null)
                            {*/
                                /*
                            string encrypted_token = "";
                            string encrypted_token_secret = "";
                            try
                            {
                                encrypted_token = PicHitMeHelper.Encrypt(accessToken);
                                encrypted_token_secret = PicHitMeHelper.Encrypt(accessTokenSecret);
                            }
                            catch (Exception e)
                            {
                                while (e.InnerException != null)
                                {
                                    e = e.InnerException;
                                }
                                throw e;
                            }*/
                                //throw new UnauthorizedAccessException("id:" + user.id + ", email:" + user.email + ", username:" + user.username + ", password:" + user.password + ", first_name:" + user.first_name + ", last_name:" + user.last_name + ", is_active:" + user.is_active);
/*                                auth_user newAuthUser = PicHitMeHelper.CreateUserEntityByLoginProviderUser(loginProvider, credentials);
                                userIdentityLink = new main_user_identity_link
                                {
                                    //auth_user = PicHitMeHelper.CreateUserEntityByLoginProviderUser(loginProvider, credentials),
                                    auth_user_id = newAuthUser.id,
                                    main_identity_provider = identityProvider,
                                    user_id = credentials.UserId,
                                    //TODO Warning! Plain token storing.
                                    token = accessToken,
                                    //token = encrypted_token,
                                    //TODO Warning! Plain token storing.
                                    token_secret = accessTokenSecret,
                                    //token_secret = encrypted_token_secret,
                                    allow_auth = true,
                                };

                                entities.main_user_identity_link.Add(userIdentityLink);
                            }
                            else if (!string.IsNullOrWhiteSpace(accessToken))
                            {
                                userIdentityLink.token = accessToken;
                                userIdentityLink.token_secret = accessTokenSecret;
                            }

#if USE_TRACE
                            using (Trace.CustomTraceWriter.TraceStartFinish(System.Reflection.MethodBase.GetCurrentMethod().Name + " SaveChanges"))
#endif
                            {
                                entities.SaveChanges();
                            }
                            // TODO Review this expression.
                            //if (!userIdentityLink.allow_auth || !userIdentityLink.auth_user.is_active)
                            if (!userIdentityLink.allow_auth)
                            {
                                throw new UnauthorizedAccessException("Not allowed");
                            }
                        }
                    }
                }
            }
        }
*/
        public static async Task<string> GetUserNameByIdentityProviderAsync(string loginProviderUserId)
        {
            // TODO Consider to use TokenHandler.TryParseUserId for parsing
            string providerName = Regex.Replace(loginProviderUserId, ":.*", "");
            if (providerName == ProviderName)
            {
                return Regex.Replace(loginProviderUserId, ".*:", "");
            }

            using (var entities = new PhmeDatabaseEntities())
            {
                main_identity_provider identityProvider = await entities.main_identity_provider.SingleOrDefaultAsync(ip => ip.name == providerName);
                if (identityProvider == null)
                {
                    return "";
                }

                    main_user_identity_link userIdentityLink = await entities.main_user_identity_link.Where(
                        uil => uil.identity_provider_id == identityProvider.id && uil.user_id == loginProviderUserId)
                        .Include(uil => uil.auth_user).SingleOrDefaultAsync();

                if (userIdentityLink == null)
                {
                    return "";
                }

                return userIdentityLink.auth_user.username;
            }
        }

        public static JToken HttpGet(string url, HttpMessageHandler handler = null)
        {
            if (handler == null)
            {
                handler = new HttpClientHandler();
            }

            using (var client = new HttpClient(handler))
            {
                var response = client.GetAsync(url).Result;
                response.EnsureSuccessStatusCode();
                return JToken.Parse(response.Content.ReadAsStringAsync().Result);
            }
        }

        public static void LogoutAndRetryIfOtherClaimsIssuer(LoginProvider loginProvider, ClaimsIdentity claimsIdentity)
        {
            if (loginProvider == null) throw new ArgumentNullException("loginProvider");
            if (claimsIdentity == null) throw new ArgumentNullException("claimsIdentity");
            var claimsChecker = loginProvider as IClaimsChecker;
            try
            {
                if (claimsChecker != null)
                {
                    claimsChecker.ThrowIfOtherClaimsIssuer(claimsIdentity);    
                }
                else
                {
                    // Default behavior is to check the provider name as issuer
                    Claim claim = claimsIdentity.FindFirst("http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier");
                    if (claim != null && claim.Issuer != loginProvider.Name)
                    {
                        throw new InvalidOperationException("Already logged in by another LoginProvider: " + claim.Issuer + ". Logged out and redirected to try again.");
                    }
                }
            }
            catch (Exception e)
            {
                GlobalLogout();
                HttpContext.Current.Response.Redirect(HttpContext.Current.Request.Url.AbsoluteUri);
                throw;
            }
        }

        public static void GlobalLogout()
        {
            IAuthenticationManager authentication = HttpContext.Current.GetOwinContext().Authentication;
            authentication.SignOut(new []
            {
                DefaultAuthenticationTypes.ApplicationCookie,
                DefaultAuthenticationTypes.ExternalBearer,
                DefaultAuthenticationTypes.ExternalCookie,
                DefaultAuthenticationTypes.TwoFactorCookie,
                DefaultAuthenticationTypes.TwoFactorRememberBrowserCookie,
                "Federation"
            });
            authentication.User = null;

            IOwinContext owinContext = HttpContext.Current.GetOwinContext();
            if (owinContext != null)
            {
                IDictionary<string, object> environment = owinContext.Environment;
                if (environment != null)
                {
                    var dictionary = environment["Microsoft.Owin.Cookies#dictionary"] as IDictionary;
                    if (dictionary != null) dictionary.Clear();
                    environment["server.User"] = null;
                }
            }

            HttpContext.Current.User = null;
            HttpContext.Current.Response.SetCookie(new HttpCookie(".AspNet.ExternalCookie", ""));
        }
    }
}
