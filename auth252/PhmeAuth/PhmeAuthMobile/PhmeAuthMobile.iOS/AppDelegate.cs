﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.WindowsAzure.MobileServices;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using PhmeAuthMobile.iOS.Auth;
using Xamarin.Forms;
using PicHitMe.iOS.Office365;

namespace PhmeAuthMobile.iOS
{
    // The UIApplicationDelegate for the application. This class is responsible for launching the 
    // User Interface of the application, as well as listening (and optionally responding) to 
    // application events from iOS.
    [Register("AppDelegate")]
    public partial class AppDelegate : UIApplicationDelegate
    {
        // class-level declarations
        UIWindow window;

        //
        // This method is invoked when the application has loaded and is ready to run. In this 
        // method you should instantiate the window, load the UI into it and then make the window
        // visible.
        //
        // You have 17 seconds to return from this method, or iOS will terminate your application.
        //
        public override bool FinishedLaunching(UIApplication app, NSDictionary options)
        {
            Forms.Init();
            
            //Important for mobile service client
            CurrentPlatform.Init();

            window = new UIWindow(UIScreen.MainScreen.Bounds);

            window.RootViewController = App.GetMainPage().CreateViewController();

            // Initialize
            var contacts = new ContactsServiceIos();
            contacts.Initialize(window.RootViewController);
            App.RemoteContacts = contacts;

            var files = new MyFilesServiceIos();
            files.Initialize(window.RootViewController);
            App.RemoteStorage = files;

            var authService = new AuthServiceClientIos();
            authService.Initialize(window.RootViewController);
            App.AuthServiceClient = authService;


            window.MakeKeyAndVisible();

            return true;
        }
    }
}
