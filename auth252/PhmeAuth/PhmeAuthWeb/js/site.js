$(document).ready(function() {
    
    var consoleBlock = $('#console-block');
    var customLogin = $('#custom-login');
    var customRegistration = $('#custom-registration');
    
    var phMeAuth = new PhMeAuth();
    
    /**
     * Facebook Login Button
     */
    $("#facebook-login-button").click(function() {
        consoleBlock.html("Login to Facebook button click");
        $('#popup-window').show();
        phMeAuth.loginToFacebook(function(result) {
            consoleBlock.html("Login successful.");
            logging(result);
        }, function(error) {
            consoleBlock.html("Login failed");
            logging(error);
        });
    });
    
    /**
     * Twitter Login Button
     */
    $("#twitter-login-button").click(function() {
        consoleBlock.html("Login to Twitter button click");
        $('#popup-window').show();
        phMeAuth.loginToTwitter(function(result) {
            consoleBlock.html("Login successful.");
            logging(result);
        }, function(error) {
            consoleBlock.html("Login failed");
            logging(error);
        });
    });
    
    /**
     * Google Login Button
     */
    $("#google-login-button").click(function() {
        consoleBlock.html("Login to Google button click");
        $('#popup-window').show();
        phMeAuth.loginToGoogle(function(result) {
            consoleBlock.html("Login successful.");
            logging(result);
        }, function(error) {
            consoleBlock.html("Login failed");
            logging(error);
        });
    });
    
    /**
     * Microsoft Login Button
     */
    $("#microsoft-login-button").click(function() {
        consoleBlock.html("Login to Microsoft Account button click");
        $('#popup-window').show();
        phMeAuth.loginToMicrosoftAccount(function(result) {
            consoleBlock.html("Login successful.");
            logging(result);
        }, function(error) {
            consoleBlock.html("Login failed");
            logging(error);
        });
    });
    
    /**
     * Azure AD Login Button
     */
    $("#aad-login-button").click(function() {
        consoleBlock.html("Login to Azure AD button click");
        $('#popup-window').show();
        phMeAuth.loginToAzureAD(function(result) {
            consoleBlock.html("Login successful.");
            logging(result);
        }, function(error) {
            consoleBlock.html("Login failed");
            logging(error);
        });
    });
    
    /**
     * Show Custom Login Button
     */
    $('#custom-login-button').click(function() {
        customLogin.toggle("slow");
    });
    
    /**
     * Custom Login Login-Button
     */
    $('#custom-login button').click(function() {
        var customLoginUsername = $('#custom-login-username').val();
        var customLoginPassword = $('#custom-login-password').val();
        consoleBlock.html("Login to PicHitMe button click");
        $('#popup-window').show();
        phMeAuth.loginToPicHitMe(customLoginUsername, customLoginPassword, function(result) {
            consoleBlock.html("Login successful.");
            logging(result);
        }, function(error) {
            consoleBlock.html("Login failed");
            logging(error);
        });
    });
    
    /**
     * Show Custom Registration Button
     */
    $('#custom-registration-button').click(function() {
        customRegistration.toggle("slow");
    });
    
    /**
     * Custom Registration Registration-Button
     */
    $('#custom-registration button').click(function() {
        var username = $('#custom-registration-username').val();
        var password = $('#custom-registration-password').val();
        var first_name = $('#custom-registration-first-name').val();
        var last_name = $('#custom-registration-last-name').val();
        var email = $('#custom-registration-email').val();
        consoleBlock.html("Register on PicHitMe button click");
        $('#popup-window').show();
        phMeAuth.registerOnPicHitMe(username, password, first_name, last_name, email, function(result) {
            consoleBlock.html("Registration successful.");
            logging(result);
        }, function(error) {
            consoleBlock.html("Registration failed");
            logging(error);
        });
    });
    
    /**
     * Logout Button
     */
    $('#logout-button').click(function() {
        consoleBlock.html("Logout button click");
        $('#popup-window').show();
        phMeAuth.logout(function(data) {
            logging(data);
        });
    });
    
    var dumpObj = function(obj) {
        var out = "";
        if(obj && typeof(obj) === "object"){
            for (var i in obj) {
                if (obj[i])
                    out += i + ": " + obj[i] + "\r\n";
            }
        } else {
            out = obj;
        }
        return out;
    };
    
    var logging = function(data) {
        $('#popup').show();
        $('#popup .result').html(dumpObj(data));
    };
    
    $('#popup a').click(function() {
        $('#popup-window').hide();
        $('#popup').hide();
    });
    
});