using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using System.Threading.Tasks;
using System.Diagnostics;
using PhmeAuthMobile;

namespace PicHitMe.iOS.Office365
{
    class MyFilesServiceIos : IRemoteStorage
    {
        private UIViewController _context;

        public async Task<bool> FolderExistsAsync(string path)
        {
            return await MyFilesApiAgent.FolderExists(path, _context);
        }

        public void Initialize(UIViewController context)
        {
            _context = context;
        }

        public async Task<List<PhmeAuthMobile.Folder>> GetRootFoldersAsync()
        {
            try
            {
                return await MyFilesApiAgent.GetRootFoldersAsync(_context);
            }
            catch(Exception ex)
            {

                Debug.WriteLine(ex.ToString());
                int i = 42;
                throw;
            }
            
        }

        public async Task<List<Folder>> GetChildrenOfAsync(Folder folder)
        {
            try
            {
                return await MyFilesApiAgent.GetChildrenOfAsync(folder, _context);
            }
            catch(Exception ex)
            {
                Debug.WriteLine(ex.ToString());
                throw;
            }
        }

        public async Task UploadFileAsync(string path, byte[] bytes)
        {
            try
            {
                await MyFilesApiAgent.UploadFileAsync(path, bytes, _context);
            }
            catch(Exception ex)
            {
                Debug.WriteLine(ex.ToString());
                throw;
            }
        }
    }
}