﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhmeAuthMobile.Auth
{
    // Should match to PhmeAuthService.Auth.RegistrationRequest
    public class RegistrationRequest
    {
        public String username { get; set; }
        public String password { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string email { get; set; }

        //TODO Add required properties if any.
    }
}
