﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Npgsql;
#if USE_PSQL
using PhmeAuthService.DataObjects.PSql;
#else
using PhmeAuthService.DataObjects.MsSql;
#endif

namespace PhmeAuthService.DataObjects
{
#if USE_PSQL
    public class NpgsqlDbConfiguration : DbConfiguration
    {
        public NpgsqlDbConfiguration()
        {
            SetProviderServices("Npgsql", NpgsqlServices.Instance);
            SetProviderFactory("Npgsql", NpgsqlFactory.Instance);
            SetExecutionStrategy("Npgsql", () => new NpgsqlExecutionStrategy());
        }
    }

    public class NpgsqlExecutionStrategy : DbExecutionStrategy
    {
        protected override bool ShouldRetryOn(Exception exception)
        {
            if (exception is InvalidOperationException)
            {
                return false;
            }

            return true;
        }
    }
#endif

    class PhmeDatabaseEntities
#if USE_PSQL
        : PhmePsqlEntities
#else
        : PhmeMssqlEntities
#endif
    {
    }
}
