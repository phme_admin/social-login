﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web.Http;
using Microsoft.WindowsAzure.Mobile.Service;
using Microsoft.WindowsAzure.Mobile.Service.Security;
using Newtonsoft.Json.Linq;
using PhmeAuthService.Auth;
using PhmeAuthService.DataObjects;

#if USE_PSQL
using PhmeAuthService.DataObjects.PSql;
#else
using PhmeAuthService.DataObjects.MsSql;
#endif

namespace PhmeAuthService.Controllers
{
    [AuthorizeLevel(AuthorizationLevel.Anonymous)]
    public class PicHitMeLoginController : ApiController
    {
        public ApiServices Services { get; set; }

        public IServiceTokenHandler handler { get; set; }

        // POST api/PicHitMeLogin
        public object Post(LoginRequest loginRequest)
        {
            try
            {
                using (var entities = new PhmeDatabaseEntities())
                {
                    Services.Log.Info("ETTT");
                    auth_user user = entities.auth_user.SingleOrDefault(a => a.username == loginRequest.username);
                    Services.Log.Info("TWO");
                    if (user != null && user.is_active)
                    {
                        if (PicHitMeHelper.MatchPicHitMeUserLogin(user, loginRequest))
                        {
                            var claimsIdentity = new ClaimsIdentity();
                            claimsIdentity.AddClaim(new Claim(ClaimTypes.NameIdentifier, loginRequest.username));
                            LoginResult loginResult = new PicHitMeLoginProvider(handler).CreateLoginResult(claimsIdentity,
                                Services.Settings.MasterKey);
                            return loginResult;
                        }
                    }

                    return this.Request.CreateResponse(HttpStatusCode.Unauthorized, "Invalid username or password");
                }
            }
            catch (Exception e)
            {
                Services.Log.Info("TRE " + e.Message);
#if DEBUG
                throw;
#endif
                // TODO handle exception content
                return Request.CreateResponse(HttpStatusCode.InternalServerError, "Error during request processing " + e.Message);
            }
        }
    }
}
