﻿namespace PhmeAuthMobile.Auth
{
    // Should match to PhmeAuthService.Auth.LoginRequest
    public class LoginRequest
    {
        public string username { get; set; }
        public string password { get; set; }
    }

    public class LoginResult
    {
        public LoginResultUser User { get; set; }
        public string AuthenticationToken { get; set; }
    }

    public class LoginResultUser
    {
        public string UserId { get; set; }
    }
}