﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Microsoft.WindowsAzure.Mobile.Service;
using Microsoft.WindowsAzure.Mobile.Service.Security;
using Newtonsoft.Json.Linq;
using PhmeAuthService.Auth;
using PhmeAuthService.DataObjects;

#if USE_PSQL
using PhmeAuthService.DataObjects.PSql;
#else
using PhmeAuthService.DataObjects.MsSql;
#endif

namespace PhmeAuthService.Controllers
{
    [AuthorizeLevel(AuthorizationLevel.User)]
    public class UserInfoController : ApiController
    {
        public ApiServices Services { get; set; }

        // GET api/userInfo
        public async Task<object> Get()
        {
            return await Post();
        }

        // POST api/userInfo
        public async Task<object> Post()
        {
            try
            {
                var user = User as ServiceUser;
#if USE_TRACE
                using (Trace.CustomTraceWriter.TraceStartFinish(System.Reflection.MethodBase.GetCurrentMethod().Name + " SUSERLEVEL: " + user.Level + ", SUSERID: " + user.Id + ", SUSERID2: " + user.Identities + ", SUSERID3: " + user.Identity))
#endif
                {
                    if (user == null)
                    {
                        return Request.CreateResponse(HttpStatusCode.Unauthorized,
                            "This can only be called by authenticated clients");
                    }

                    if (user.Level != AuthorizationLevel.User || user.Id == null)
                    {
                        return Request.CreateResponse(HttpStatusCode.Unauthorized,
                            "This can only be called by AuthorizationLevel.User clients");
                    }
                }
                string providerName = Regex.Replace(user.Id.ToString(), ":.*", "");
                string providerId = Regex.Replace(user.Id.ToString(), ".*:", "");

                string VOID_URL = "https://pichitmedev.com/api/users_identity_provider/" + HttpUtility.UrlEncode(providerName) + "/" + HttpUtility.UrlEncode(providerId) + "/";

#if USE_TRACE
                using (Trace.CustomTraceWriter.TraceStartFinish(System.Reflection.MethodBase.GetCurrentMethod().Name + " GET " + VOID_URL))
#endif
                {
                    HttpWebRequest objRequest = (HttpWebRequest)WebRequest.Create(VOID_URL);

                    // returned values are returned as a stream, then read into a string
                    HttpWebResponse objResponse = (HttpWebResponse)objRequest.GetResponse();
                    //Stream resStream = objResponse.GetResponseStream();
                    using (StreamReader responseStream = new StreamReader(objResponse.GetResponseStream()))
                    {
                        string get_response = responseStream.ReadToEnd();
                        JObject jObject = JObject.Parse(get_response);
                        string username = jObject.Value<string>("username");
                        //string password = jObject.Value<string>("password");
                       //responseStream.Close();
                        if (username == null)
                        {
                            return Request.CreateResponse(HttpStatusCode.Unauthorized, "Unknown user");
                        }
                        return jObject; 
                    }
                }
            }
            catch (Exception)
            {
#if DEBUG
                throw;
#endif
                // TODO handle exception content
                return Request.CreateResponse(HttpStatusCode.InternalServerError, "Error during request processing");
            }
        }

        // POST api/userInfo
/*        public async Task<object> Post()
        {
            try
            {
                var user = User as ServiceUser;
                if (user == null)
                {
                    return Request.CreateResponse(HttpStatusCode.Unauthorized,
                        "This can only be called by authenticated clients");
                }

                if (user.Level != AuthorizationLevel.User || user.Id == null)
                {
                    return Request.CreateResponse(HttpStatusCode.Unauthorized,
                        "This can only be called by AuthorizationLevel.User clients");
                }

                using (var entities = new PhmeDatabaseEntities())
                {
                    entities.Configuration.LazyLoadingEnabled = false;
                    string username = await PicHitMeLoginProvider.GetUserNameByIdentityProviderAsync(user.Id);
                    auth_user authUser = await entities.auth_user.FirstOrDefaultAsync(u => u.username == username);
                    if (authUser == null)
                    {
                        return Request.CreateResponse(HttpStatusCode.Unauthorized, "Unknown user");
                    }

                    return authUser;
                }
            }
            catch (Exception)
            {
#if DEBUG
                throw;
#endif
                // TODO handle exception content
                return Request.CreateResponse(HttpStatusCode.InternalServerError, "Error during request processing");
            }
        }*/

    }
}