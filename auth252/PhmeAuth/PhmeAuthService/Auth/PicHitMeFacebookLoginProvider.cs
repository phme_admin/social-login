﻿using System;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web.Http;
using Microsoft.WindowsAzure.Mobile.Service.Security;
using Microsoft.WindowsAzure.Mobile.Service.Security.Providers;
using Newtonsoft.Json.Linq;
using Microsoft.WindowsAzure.Mobile.Service;

namespace PhmeAuthService.Auth
{
    public class PicHitMeFacebookLoginProvider
        : FacebookLoginProvider, IUserInfoProvider
    {
        public PicHitMeFacebookLoginProvider(HttpConfiguration config, IServiceTokenHandler tokenHandler)
            : base(config, tokenHandler)
        {
        }

        public JObject GetUserInfo(ProviderCredentials credentials)
        {
            var result = new JObject();
            var facebookCredentials = credentials as FacebookCredentials;
            if (facebookCredentials != null)
            {
                result.Add(Name,
                    PicHitMeLoginProvider.HttpGet("https://graph.facebook.com/me?access_token=" +
                                                   facebookCredentials.AccessToken));
            }

            return result;
        }

        public RegistrationRequest CreateRegistrationRequestByUserInfo(JObject userInfo)
        {
            JToken info = userInfo.GetValue(Name);
            var registrationRequest = new RegistrationRequest
            {
                email = info.Value<string>("email") ?? "",
                first_name = info.Value<string>("first_name") ?? "",
                last_name = info.Value<string>("last_name") ?? ""
            };

            return registrationRequest;
        }

        public override ProviderCredentials CreateCredentials(ClaimsIdentity claimsIdentity)
        {
#if USE_TRACE
            using (Trace.CustomTraceWriter.TraceStartFinish(System.Reflection.MethodBase.GetCurrentMethod()))
#endif
            {
                PicHitMeLoginProvider.LogoutAndRetryIfOtherClaimsIssuer(this, claimsIdentity);

                var facebookCredentials = base.CreateCredentials(claimsIdentity) as FacebookCredentials;
                if (facebookCredentials != null)
                {
                    PicHitMeLoginProvider.HandleIdentityProviderUser(this, facebookCredentials,
                        facebookCredentials.AccessToken);
                }

                return facebookCredentials;
            }
        }
    }
}