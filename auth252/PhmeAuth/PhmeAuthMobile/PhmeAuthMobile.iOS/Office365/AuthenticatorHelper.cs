using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using Microsoft.Office365.OAuth;
using Xamarin.Auth;
using Authenticator = Microsoft.Office365.OAuth.Authenticator;

namespace PicHitMe.iOS.Office365
{
    public static class AuthenticatorHelper
    {
        private static Authenticator _authenticator;

        public static async Task<AuthenticationInfo> GetAuthenticationInfo(UIViewController context, bool reauthenticate, string serviceId, ServiceIdentifierKind idKind = ServiceIdentifierKind.Resource)
        {
            AuthenticationInfo authInfo = null;
            Authenticator authenticator = GetAuthenticator(context);
            
            /**** This code changes the key using for account storage */
            Type accountManagerType = authenticator.GetType().Assembly.GetType("Microsoft.Office365.OAuth.AccountManager");
            FieldInfo idField = accountManagerType.GetField("ServiceId", BindingFlags.NonPublic | BindingFlags.Static);
            FieldInfo idUserField = accountManagerType.GetField("ServiceIdUser", BindingFlags.NonPublic | BindingFlags.Static);
            MethodInfo loadMethod = accountManagerType.GetMethod("Load", BindingFlags.NonPublic | BindingFlags.Static);
            if (idField != null && idUserField != null && loadMethod != null)
            {
                string id = "PicHitMe.iOS.Office365.Id" + serviceId;
                string idUser = "PicHitMe.iOS.Office365.IdUser" + serviceId;

                idField.SetValue(null, id);
                idUserField.SetValue(null, idUser);
                loadMethod.Invoke(null, null);
            }
            /**********************************************************/

            if (reauthenticate)
            {
                authenticator.ClearCache();
            }

            int retryCount = 2;

            while (retryCount > 0)
            {
                try
                {
                    authInfo = await authenticator.AuthenticateAsync(serviceId, idKind);
                    break;
                }
                catch (Exception ex)
                {
                    authenticator.ClearCache();
                    retryCount--;

                    if (retryCount == 0)
                    {
                        // I give up...
                        throw;
                    }
                }
            }

            return authInfo;
        }

        private static Authenticator GetAuthenticator(UIViewController context)
        {
            try
            {
                if (_authenticator == null)
                {
                    _authenticator = new Authenticator(context);
                }

                return _authenticator;
            }
            catch(Exception ex)
            {
                int i = 42;
                throw;
            }
        }
    }
}