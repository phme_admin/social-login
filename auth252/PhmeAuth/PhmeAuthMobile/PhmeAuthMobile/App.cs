﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PhmeAuthMobile.Auth;
using Xamarin.Forms;

namespace PhmeAuthMobile
{
    public class App
    {
        public static Page GetMainPage()
        {
            Navigation = new NavigationPage(new MainPage());
            return Navigation;
        }

        public static IRemoteContacts RemoteContacts { get; set; }
        public static IRemoteStorage RemoteStorage { get; set; }
        public static AuthServiceClient AuthServiceClient { get; set; }
        public static NavigationPage Navigation { get; set; }
    }
}
