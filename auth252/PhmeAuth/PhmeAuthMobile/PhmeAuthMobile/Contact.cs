﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhmeAuthMobile
{
    public class Contact
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }

        // Ok so this might be the wrong place to track this, but for demo, it's ok
        public bool Selected { get; set; }
    }
}
