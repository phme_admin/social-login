﻿using MonoTouch.UIKit;
using Microsoft.Office365.Exchange;
using Microsoft.Office365.OAuth;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PicHitMe.iOS.Office365
{
    static class ContactsAPIAgent
    {
        static ExchangeClient _client;

        const string ExchangeResourceId = "https://outlook.office365.com";
        const string ExchangeServiceRoot = "https://outlook.office365.com/ews/odata";

        public static async Task<IEnumerable<IContact>> GetContacts(UIViewController context)
        {
            bool firstTry = true;

            while (true)
            {
                try
                {
                    // if (_client == null)
                    // {
                    _client = await EnsureClientCreated(context, !firstTry);
                    // }

                    // Obtain first page of contacts
                    var contactsResults = await (from i in _client.Me.Contacts
                                                 orderby i.DisplayName
                                                 select i).ExecuteAsync();

                    return contactsResults.CurrentPage;
                }
                catch (Exception ex)
                {
                    if (firstTry == false)
                        throw;

                    firstTry = false;
                }
            }
        }
    
        public static async Task<ExchangeClient> EnsureClientCreated(UIViewController context, bool reauthenticate)
        {
            AuthenticationInfo authInfo = await AuthenticatorHelper.GetAuthenticationInfo(context, reauthenticate, ExchangeResourceId);
            // TODO Handle the null return of authInfo here
            return new ExchangeClient(new Uri(ExchangeServiceRoot), authInfo.GetAccessToken);
        }

        public static async Task<byte[]> GetContactPicture(string strContactId, UIViewController context)
        {
          //  if(_client==null)
         //   {
                _client = await EnsureClientCreated(context, false);
           // }

            byte[] bytesContactPhoto = new byte[0];
            var contact = await _client.Me.Contacts[strContactId].ExecuteAsync();
            var attachmentResult = await ((Microsoft.Office365.Exchange.IContactFetcher)contact).Attachments.ExecuteAsync();
            var attachments = attachmentResult.CurrentPage.ToArray();
            var contactPhotoAttachment = attachments.OfType<IFileAttachment>().FirstOrDefault(a => a.IsContactPhoto);
            if (contactPhotoAttachment != null)
            {
                bytesContactPhoto = contactPhotoAttachment.ContentBytes;
            }
            return bytesContactPhoto;
        }
    }
}
