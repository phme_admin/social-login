SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [auth_user](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[username] [nvarchar](255) NOT NULL,
	[first_name] [nvarchar](30) NOT NULL,
	[last_name] [nvarchar](30) NOT NULL,
	[email] [nvarchar](75) NOT NULL,
	[password] [nvarchar](128) NOT NULL,
	[is_active] [bit] NOT NULL,
 CONSTRAINT [PK_auth_user] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF)
)

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [main_identity_provider](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](255) NOT NULL,
 CONSTRAINT [PK_main_identity_provider] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF)
)

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [main_user_identity_link](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[auth_user_id] [int] NOT NULL,
	[main_identity_provider_id] [int] NOT NULL,
	[user_id] [nvarchar](255) NOT NULL,
	[token] [nvarchar](max) NULL,
	[allow_auth] [bit] NOT NULL,
	[token_secret] [nvarchar](max) NULL,
 CONSTRAINT [PK_main_user_identity_link] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF)
)

GO
SET ANSI_PADDING ON

GO
CREATE UNIQUE NONCLUSTERED INDEX [UNIQUE_main_user_identity_link] ON [main_user_identity_link]
(
	[auth_user_id] ASC,
	[main_identity_provider_id] ASC,
	[user_id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF)
GO
ALTER TABLE [main_user_identity_link] ADD  CONSTRAINT [DF_main_user_identity_link_allow_auth]  DEFAULT ((0)) FOR [allow_auth]
GO
ALTER TABLE [main_user_identity_link]  WITH CHECK ADD  CONSTRAINT [FK_main_user_identity_link_auth_user] FOREIGN KEY([auth_user_id])
REFERENCES [auth_user] ([id])
GO
ALTER TABLE [main_user_identity_link] CHECK CONSTRAINT [FK_main_user_identity_link_auth_user]
GO
ALTER TABLE [main_user_identity_link]  WITH CHECK ADD  CONSTRAINT [FK_main_user_identity_link_main_identity_provider] FOREIGN KEY([main_identity_provider_id])
REFERENCES [main_identity_provider] ([id])
GO
ALTER TABLE [main_user_identity_link] CHECK CONSTRAINT [FK_main_user_identity_link_main_identity_provider]
GO
