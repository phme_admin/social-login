﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhmeAuthMobile
{
    public class Folder
    {
        public Folder()
        {
            Children = new List<Folder>();
        }

        public string Id { get; set; }

        public string Name { get; set; }

        public List<Folder> Children { get; set; }

        public Folder Parent { get; set; }

    }
}
