﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace PhmeAuthMobile
{
    public partial class MainPage
    {
        public MainPage()
        {
            InitializeComponent();

            //ContactsButton.Clicked += ContactsButton_Clicked;
            //FilesButton.Clicked += FilesButton_Clicked;

            RegistrationButton.Clicked += RegistrationButton_Clicked;
            PicHitMeButton.Clicked += PicHitMeButton_Clicked;
            MicrosoftButton.Clicked += MicrosoftButton_Clicked;
            AzureAdButton.Clicked += AzureAdButton_Clicked;
            FacebookButton.Clicked += FacebookButton_Clicked;
            GoogleButton.Clicked += GoogleButton_Clicked;
            TwitterButton.Clicked += TwitterButton_Clicked;
        }

        async void FilesButton_Clicked(object sender, EventArgs e)
        {
            try
            {
                Status.Text = "Calling MyFiles to get folders";
                var folders = await App.RemoteStorage.GetRootFoldersAsync();

                Status.Text = "Call returned";
                await DisplayAlert("SUCCESS", string.Format("Got {0} folders", folders.Count()), "OK", "Cancel");
            }
            catch (Exception ex)
            {
                DisplayAlert("ERROR", ex.ToString(), "OK", "OK");
            }
        }

        async void ContactsButton_Clicked(object sender, EventArgs e)
        {
            try
            {
                Status.Text = "Calling Contacts to get... well contacts...";
                var contacts = await App.RemoteContacts.GetContactsAsync();

                Status.Text = "Call returned";
                await DisplayAlert("SUCCESS", string.Format("Got {0} contacts", contacts.Count()), "OK", "Cancel");
            }
            catch (Exception ex)
            {
                DisplayAlert("ERROR", ex.ToString(), "OK", "OK");
                return;
            }
        }

        async void PicHitMeButton_Clicked(object sender, EventArgs e)
        {
            await App.Navigation.PushAsync(new PicHitMeLoginPage());
        }

        async void RegistrationButton_Clicked(object sender, EventArgs e)
        {
            await App.Navigation.PushAsync(new PicHitMeRegistrationPage());
        }

        async void MicrosoftButton_Clicked(object sender, EventArgs e)
        {
            await ExecuteLoginAsync("Microsoft Account", async () => await App.AuthServiceClient.LoginToMicrosoftAccountAsync());
        }

        async void AzureAdButton_Clicked(object sender, EventArgs e)
        {
            await ExecuteLoginAsync("Azure Active Directory", async () => await App.AuthServiceClient.LoginToWindowsAzureActiveDirectoryAsync());
        }

        async void FacebookButton_Clicked(object sender, EventArgs e)
        {
            await ExecuteLoginAsync("Facebook", async () => await App.AuthServiceClient.LoginToFacebookAsync());
        }

        async void GoogleButton_Clicked(object sender, EventArgs e)
        {
            await ExecuteLoginAsync("Google", async () => await App.AuthServiceClient.LoginToGoogleAsync());
        }

        async void TwitterButton_Clicked(object sender, EventArgs e)
        {
            await ExecuteLoginAsync("Twitter", async () => await App.AuthServiceClient.LoginToTwitterAsync());
        }

        private async Task ExecuteLoginAsync(string name, Func<Task> action)
        {
            if (name == null) throw new ArgumentNullException("name");
            if (action == null) throw new ArgumentNullException("action");
            await ExecuteActionAsync("Logging in via " + name, action, this, Status);
        }

        public static async Task ExecuteActionAsync(string caption, Func<Task> action, Page page, Label status = null, bool getUserInfo = true)
        {
            if (page == null) throw new ArgumentNullException("page");
            try
            {
                if (caption == null) throw new ArgumentNullException("caption");
                if (action == null) throw new ArgumentNullException("action");
                if (status != null) status.Text = caption + "...";
                await action();
                if (status != null) status.Text = "Success";

                if (getUserInfo)
                {
                    string userInfo = (await App.AuthServiceClient.GetUserInfo()).ToString();
                    await page.DisplayAlert("User info", string.Format("Auth_user: {0}", userInfo), "OK");
                }
                else
                {
                    await page.DisplayAlert("Success", "Success", "OK");
                }
            }
            catch (Exception ex)
            {
                if (status != null) status.Text = "Error";
                page.DisplayAlert("ERROR", ex.Message, "OK");
            }
        }
    }
}
