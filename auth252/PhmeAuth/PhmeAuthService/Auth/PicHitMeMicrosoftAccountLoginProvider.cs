﻿using System;
using System.Net;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web.Http;
using Microsoft.WindowsAzure.Mobile.Service.Security;
using Microsoft.WindowsAzure.Mobile.Service.Security.Providers;
using Newtonsoft.Json.Linq;

namespace PhmeAuthService.Auth
{
    public class PicHitMeMicrosoftAccountLoginProvider
        : MicrosoftAccountLoginProvider, IUserInfoProvider
    {
        public PicHitMeMicrosoftAccountLoginProvider(HttpConfiguration config, IServiceTokenHandler tokenHandler)
            : base(config, tokenHandler)
        {
        }

        public JObject GetUserInfo(ProviderCredentials credentials)
        {
            var result = new JObject();
            var microsoftAccountCredentials = credentials as MicrosoftAccountCredentials;
            if (microsoftAccountCredentials != null)
            {
                result.Add(Name,
                    PicHitMeLoginProvider.HttpGet("https://apis.live.net/v5.0/me/?method=GET&access_token=" +
                                                   microsoftAccountCredentials.AccessToken));
            }

            return result;
        }

        public RegistrationRequest CreateRegistrationRequestByUserInfo(JObject userInfo)
        {
            JToken info = userInfo.GetValue(Name);
            JToken email = info.SelectToken("emails.account");
            var registrationRequest = new RegistrationRequest
            {
                email = email == null ? "" : email.ToString(),
                first_name = info.Value<string>("first_name") ?? "",
                last_name = info.Value<string>("last_name") ?? ""
            };

            return registrationRequest;
        }

        public override ProviderCredentials CreateCredentials(ClaimsIdentity claimsIdentity)
        {
#if USE_TRACE
            using (Trace.CustomTraceWriter.TraceStartFinish(System.Reflection.MethodBase.GetCurrentMethod()))
#endif
            {
                PicHitMeLoginProvider.LogoutAndRetryIfOtherClaimsIssuer(this, claimsIdentity);

                var microsoftAccountCredentials = base.CreateCredentials(claimsIdentity) as MicrosoftAccountCredentials;
                if (microsoftAccountCredentials != null)
                {
                    PicHitMeLoginProvider.HandleIdentityProviderUser(this, microsoftAccountCredentials,
                        microsoftAccountCredentials.AccessToken);
                }

                return microsoftAccountCredentials;
            }
        }
    }
}