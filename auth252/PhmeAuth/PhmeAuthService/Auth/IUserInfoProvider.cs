using System.Net;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.WindowsAzure.Mobile.Service.Security;
using Newtonsoft.Json.Linq;

namespace PhmeAuthService.Auth
{
    public interface IUserInfoProvider
    {
        JObject GetUserInfo(ProviderCredentials credentials);
        RegistrationRequest CreateRegistrationRequestByUserInfo(JObject userInfo);
    }

    public interface IClaimsChecker
    {
        void ThrowIfOtherClaimsIssuer(ClaimsIdentity claimsIdentity);
    }
}