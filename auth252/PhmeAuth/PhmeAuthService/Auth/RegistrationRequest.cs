﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhmeAuthService.Auth
{
    public class RegistrationRequest
    {
        public String username { get; set; }
        public String password { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string email { get; set; }
        public string token { get; set; }
        public string token_secret { get; set; }
        public string identity_provider { get; set; }
        public string identity_provider_user_id { get; set; }

    }
}
