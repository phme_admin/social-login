﻿//using MonoTouch.UIKit;
//using Microsoft.Office365.OAuth;
//using Microsoft.Office365.SharePoint;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;

//namespace PhmeAuthMobile.iOS
//{
//    static class  MyFilesApiSample
//    {
//        const string MyFilesCapability = "MyFiles";

//        public static async Task<IEnumerable<IFileSystemItem>> GetMyFiles(UIViewController context)
//        {
//            var client = await EnsureClientCreated(context);

//            // Obtain files in folder "Shared with Everyone"
//            var filesResults = await client.Files["Shared with Everyone"].ToFolder().Children.ExecuteAsync();
//            var files = filesResults.CurrentPage.OrderBy(e => e.Name);

//            return files;
//        }
    
//        public static async Task<SharePointClient> EnsureClientCreated(UIViewController context)
//        {
//            Authenticator authenticator = new Authenticator(context);
//            var authInfo = await authenticator.AuthenticateAsync(MyFilesCapability, ServiceIdentifierKind.Capability);

//            // Create the MyFiles client proxy:
//            return new SharePointClient(authInfo.ServiceUri, authInfo.GetAccessToken);
//        }
//    }
//}
