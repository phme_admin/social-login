﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Tracing;
using Microsoft.WindowsAzure.Mobile.Service.Security.Providers;
using Newtonsoft.Json.Serialization;
using PhmeAuthService.Auth;
using PhmeAuthService.DataObjects;
using PhmeAuthService.Models;
using Microsoft.WindowsAzure.Mobile.Service;
using ITraceWriter = System.Web.Http.Tracing.ITraceWriter;

namespace PhmeAuthService
{
    public static class WebApiConfig
    {
        public static void Register()
        {
            // Use this class to set configuration options for your mobile service
            ConfigOptions options = new ConfigOptions();

            options.LoginProviders.Clear();
            options.LoginProviders.Add(typeof(PicHitMeFacebookLoginProvider));
            options.LoginProviders.Add(typeof(PicHitMeMicrosoftAccountLoginProvider));
            options.LoginProviders.Add(typeof(PicHitMeGoogleLoginProvider));
            options.LoginProviders.Add(typeof(PicHitMeTwitterLoginProvider));
            options.LoginProviders.Add(typeof(PicHitMeAzureAdLoginProvider));
            
            // Use this class to set WebAPI configuration options
            HttpConfiguration config = ServiceConfig.Initialize(new ConfigBuilder(options));

            // Unnesessary interface for now. Remove this line if jobs are required.
            config.Routes.Remove("jobs");

            // Camel property naming for JSON
            config.Formatters.JsonFormatter.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();

            // Using JSON as a main output serializer by disabling the xml one
            config.Formatters.XmlFormatter.SupportedMediaTypes.Clear();

            // To display errors in the browser during development, uncomment the following
            // line. Comment it out again when you deploy your service for production use.
#if DEBUG
            config.IncludeErrorDetailPolicy = IncludeErrorDetailPolicy.Always;
#endif

#if USE_TRACE
            ITraceWriter originalTraceWriter = null;
            if (config.GetIsHosted())
            {
                originalTraceWriter = (ITraceWriter)config.Services.GetService(typeof(ITraceWriter));
            }

            config.Services.Replace(typeof(ITraceWriter), new Trace.CustomTraceWriter(originalTraceWriter));
#endif
        }
    }
}

