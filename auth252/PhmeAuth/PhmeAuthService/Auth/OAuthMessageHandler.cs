﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Http;
using OAuth;
using System.Threading.Tasks;

namespace OAuthMessageHandler.Core {

    public class OAuthMessageHandler : DelegatingHandler {

        private string _consumerKey;
        private string _consumerSecret;
        private string _token;
        private string _tokenSecret;

        private readonly OAuthBase _oauthBase = new OAuthBase();

        public OAuthMessageHandler(string consumerKey, string consumerSecret, string token, string tokenSecret,
            HttpMessageHandler innerHandler)
            : base(innerHandler)
        {
            _consumerKey = consumerKey;
            _consumerSecret = consumerSecret;
            _token = token;
            _tokenSecret = tokenSecret;
        }

        protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, System.Threading.CancellationToken cancellationToken) {

            // Compute OAuth header
            string normalizedUri;
            string normalizedParameters;
            string authHeader;

            _oauthBase.GenerateSignature(
                request.RequestUri,
                _consumerKey,
                _consumerSecret,
                _token,
                _tokenSecret,
                request.Method.Method,
                _oauthBase.GenerateTimeStamp(),
                _oauthBase.GenerateNonce(),
                out normalizedUri,
                out normalizedParameters,
                out authHeader);

            request.Headers.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("OAuth", authHeader);
            return base.SendAsync(request, cancellationToken);
        }
    }
}