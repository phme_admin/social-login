﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Microsoft.WindowsAzure.Mobile.Service;
using Microsoft.WindowsAzure.Mobile.Service.Security;
using PhmeAuthService.Auth;
using PhmeAuthService.DataObjects;

#if USE_PSQL
using PhmeAuthService.DataObjects.PSql;
#else
using PhmeAuthService.DataObjects.MsSql;
#endif

namespace PhmeAuthService.Controllers
{
    [AuthorizeLevel(AuthorizationLevel.Anonymous)]
    public class PicHitMeRegistrationController : ApiController
    {
        public ApiServices Services { get; set; }

        // POST api/PicHitMeRegistration
        public HttpResponseMessage Post(RegistrationRequest registrationRequest)
        {
            try
            {
                using (var entities = new PhmeDatabaseEntities())
                {
                    auth_user user = entities.auth_user.SingleOrDefault(a => a.username == registrationRequest.username);
                    if (user != null)
                    {
                        return Request.CreateResponse(HttpStatusCode.BadRequest, "Username already exists");
                    }

                    try
                    {
                        //auth_user newUser = PicHitMeHelper.CreateUserEntity(registrationRequest);
                        //entities.auth_user.Add(newUser);
                    }
                    catch (Exception e)
                    {
                        return Request.CreateResponse(HttpStatusCode.BadRequest, "BadRequest in PicHitMeRegistrationController: " + e.Message);
                    }

                    entities.SaveChanges();
                    return Request.CreateResponse(HttpStatusCode.Created, "User is created");
                }
            }
            catch (Exception)
            {
#if DEBUG
                throw;
#endif
                // TODO handle exception content
                return Request.CreateResponse(HttpStatusCode.InternalServerError, "Error during request processing");
            }
        }

    }
}