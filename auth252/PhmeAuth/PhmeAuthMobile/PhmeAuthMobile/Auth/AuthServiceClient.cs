﻿using System;
using System.Threading.Tasks;
using Microsoft.WindowsAzure.MobileServices;
using Newtonsoft.Json.Linq;

namespace PhmeAuthMobile.Auth
{
    public abstract class AuthServiceClient : IAuthServiceClient
    {
        private readonly MobileServiceClient _mobileServiceClient =
            new MobileServiceClient(
                "https://pichitmeauth.azure-mobile.net/", 
                "MnmCVQjRKXhyzmUUXmqISpsutqSKPH89");

        public async Task<MobileServiceUser> LoginToFacebookAsync()
        {
            return await ReLoginAsync(MobileServiceAuthenticationProvider.Facebook);
        }

        public async Task<MobileServiceUser> LoginToTwitterAsync()
        {
            return await ReLoginAsync(MobileServiceAuthenticationProvider.Twitter);
        }

        public async Task<MobileServiceUser> LoginToGoogleAsync()
        {
            return await ReLoginAsync(MobileServiceAuthenticationProvider.Google);
        }

        public async Task<MobileServiceUser> LoginToMicrosoftAccountAsync()
        {
            return await ReLoginAsync(MobileServiceAuthenticationProvider.MicrosoftAccount);
        }

        public async Task<MobileServiceUser> LoginToWindowsAzureActiveDirectoryAsync()
        {
            return await ReLoginAsync(MobileServiceAuthenticationProvider.WindowsAzureActiveDirectory);
        }

        public async Task<MobileServiceUser> LoginToPicHitMeAsync(string username, string password)
        {
            if (username == null) throw new ArgumentNullException("username");
            if (password == null) throw new ArgumentNullException("password");

            await Logout();
            var loginRequest = new LoginRequest {username = username, password = password};
            LoginResult loginResult =
                await Client.InvokeApiAsync<LoginRequest, LoginResult>("PicHitMeLogin", loginRequest);
            return Client.CurrentUser = new MobileServiceUser(loginResult.User.UserId)
            {
                MobileServiceAuthenticationToken = loginResult.AuthenticationToken
            };
        }

        public async Task<JToken> GetUserInfo()
        {
            return await Client.InvokeApiAsync<JToken>("userInfo");
        }

        public MobileServiceUser User
        {
            get { return Client.CurrentUser; }
        }

        public async Task<string> RegisterOnPicHitMeAsync(string username, string password, string firstName, string lastName, string email)
        {
            //TODO Verify method parameters?

            await Logout();
            var registrationRequest = new RegistrationRequest
            {
                username = username, 
                password = password,
                email = email,
                first_name = firstName,
                last_name = lastName
            };

            string result =
                await Client.InvokeApiAsync<RegistrationRequest, string>("PicHitMeRegistration", registrationRequest);
            return result;
        }

        public async Task Logout()
        {
            if (Client.CurrentUser != null)
            {
                await Client.InvokeApiAsync("Logout");
                Client.Logout();
            }
        }

        public MobileServiceClient Client
        {
            get { return _mobileServiceClient; }
        }

        private async Task<MobileServiceUser> ReLoginAsync(MobileServiceAuthenticationProvider provider)
        {
            await Logout();
            return await LoginAsync(provider);
        }

        protected abstract Task<MobileServiceUser> LoginAsync(MobileServiceAuthenticationProvider provider);
    }
}