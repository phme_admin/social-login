﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhmeAuthMobile
{
    public interface IRemoteStorage
    {
        Task<bool> FolderExistsAsync(string path);

        Task<List<Folder>> GetRootFoldersAsync();

        Task<List<Folder>> GetChildrenOfAsync(Folder folder);

        Task UploadFileAsync(string path, Byte[] bytes);

    }
}
