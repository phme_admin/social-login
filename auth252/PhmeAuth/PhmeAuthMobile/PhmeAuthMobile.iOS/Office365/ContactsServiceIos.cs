using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using System.Threading.Tasks;
using System.Reflection;
using PhmeAuthMobile;

namespace PicHitMe.iOS.Office365
{
    public class ContactsServiceIos : IRemoteContacts
    {
        private UIViewController _context;

        public void Initialize(UIViewController context)
        {
            _context = context;
        }

        public async System.Threading.Tasks.Task<IEnumerable<Contact>> GetContactsAsync()
        {
           // await ContactsAPIAgent.EnsureClientCreated(_context);
            var contacts = await ContactsAPIAgent.GetContacts(_context);

            var result = new List<Contact>();
            foreach(var contact in contacts)
            {
                result.Add(new Contact()
                    {
                        Id = contact.Id,
                        Name = contact.GivenName + " " + contact.Surname,
                        Email = contact.EmailAddress1
                    });
            }

            return result;
        }
        public async Task<byte[]> GetAvatarAsync(string contactId)
        {
            try
            {
                var bytes = await ContactsAPIAgent.GetContactPicture(contactId, _context);
                return bytes;
            }
            catch(Exception ex)
            {
                int i = 42;
                throw;
            }
        }




        public byte[] DefaultAvatarImage()
        {
            //return ResourceLoader.GetEmbeddedResourceBytes(Assembly.GetExecutingAssembly(), "DefaultContact.png");
            return null;
        }
    }
}