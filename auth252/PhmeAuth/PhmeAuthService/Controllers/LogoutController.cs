﻿using System.Net;
using System.Net.Http;
using System.Web.Http;
using Microsoft.WindowsAzure.Mobile.Service;
using Microsoft.WindowsAzure.Mobile.Service.Security;
using PhmeAuthService.Auth;

namespace PhmeAuthService.Controllers
{
    [AuthorizeLevel(AuthorizationLevel.Anonymous)]
    public class LogoutController : ApiController
    {
        public ApiServices Services { get; set; }

        // GET api/Logout
        public HttpResponseMessage Get()
        {
            return Post();
        }

        // POST api/Logout
        public HttpResponseMessage Post()
        {
            PicHitMeLoginProvider.GlobalLogout();
            return Request.CreateResponse(HttpStatusCode.OK, "Logged out");
        }
    }
}