﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhmeAuthMobile
{
    public interface IRemoteContacts
    {
        Task<IEnumerable<Contact>> GetContactsAsync();

        Task<Byte[]> GetAvatarAsync(string contactId);

        /// <summary>
        /// Returns the image that should be displayed before the actual profile image is loaded
        /// </summary>
        /// <returns></returns>
        Byte[] DefaultAvatarImage();
    }
}
