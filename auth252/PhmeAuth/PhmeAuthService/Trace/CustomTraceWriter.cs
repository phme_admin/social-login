﻿#if USE_TRACE
using System;
using System.Net.Http;
using System.Reflection;
using System.Web.Http;
using System.Web.Http.Tracing;

namespace PhmeAuthService.Trace
{
    internal class CustomTraceWriter : ITraceWriter
    {
        private static CustomTraceWriter LastInstance { get; set; }
        
        private readonly ITraceWriter _original;

        public CustomTraceWriter(ITraceWriter original)
        {
            _original = original;
            LastInstance = this;
        }

        public void Trace(HttpRequestMessage request, string category, TraceLevel level, Action<TraceRecord> traceAction)
        {
            if (_original != null)
            {
                _original.Trace(request, category, level, traceAction);
            }

            var record = new TraceRecord(request, category, level);
            traceAction(record);
            System.Diagnostics.Trace.WriteLine(string.Format("{0} - {1} - {2} - {3}",
                record.Level, record.Message, record.Category, record.Timestamp.ToString("ddd MMM dd yyyy, HH:mm:ss tt")));
        }

        public void Start(string message)
        {
            this.Info("Start: " + message);
        }

        public void Finish(string message)
        {
            this.Info("Finish: " + message);
        }

        public static IDisposable TraceStartFinish(string message)
        {
            return new StartFinish(message, LastInstance);
        }

        public static IDisposable TraceStartFinish(MethodBase method)
        {
            return TraceStartFinish(method.ReflectedType == null ? method.Name : (method.ReflectedType.Name + "." + method.Name));
        }
    }

    internal class StartFinish : IDisposable
    {
        private CustomTraceWriter _traceWriter;
        private readonly string _message;

        public StartFinish(string message, CustomTraceWriter traceWriter)
        {
            if (traceWriter == null) throw new ArgumentNullException("traceWriter");
            _message = message;
            _traceWriter = traceWriter;
            _traceWriter.Start(_message);
        }

        public void Dispose()
        {
            if (_traceWriter != null)
            {
                _traceWriter.Finish(_message);
                _traceWriter = null;
            }
        }
    }
}
#endif