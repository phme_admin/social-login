﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.WindowsAzure.Mobile.Service.Security;

namespace PhmeAuthService.Auth
{
    public class PicHitMeProviderCredentials : ProviderCredentials
    {
        public PicHitMeProviderCredentials()
            : base(PicHitMeLoginProvider.ProviderName)
        {
        }
    }
}
